Story-03
Feature: Response is sent
	User should be able to send response
	Sent response should be available in Send category
Background:
	Given MyNjuJob user is logged
	And gmail homepage is displayed
	And emails are available in the mailbox
	
Scenario1: Response to email is sent
	Given email in the mailbox is selected
	When I click 'response' action
	And I insert 'my response' content in the active dialog
	And I click 'send' button
	Then I see poped-up message 'email has been sent'
	And the message is received in my mailbox
	
