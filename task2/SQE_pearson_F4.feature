Story-04
Feature: Email is deleted 
	User should be able to delete email
	Deleted email is removed from the mailbox
Background:
	Given MyNjuJob user is logged
	And gmail homepage is displayed
	And emails are available in the mailbox
	
Scenario1: Email is deleted from the mailbox
 	Given email in the mailbox is selected
	And the toolbar is active
	When I click 'delete' action
	Then I see poped-up message 'email has been removed'
	And the message is removed from my mailbox

