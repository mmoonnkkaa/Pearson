Story-05
Feature: Search for emails with desired content
	User should be able to fill in search bar
	User should be able to see searching results
Background:
	Given MyNjuJob user is logged
	And gmail homepage is displayed
	And several emails with desired content are available in the mailbox
	
Scenario Outline: 
  Given the <search> input
  When we fill in the search field
  Then the result <result> should be selected
  And the emails with <result> in content should be displayed

  Examples:
    | 				 search | result 			 |
    |         		   test | test				 |
    |   			pearson | pearson			 |
    |    rekrutacja_pearson | rekrutacja_pearson |
    |                   123 | 123 				 |