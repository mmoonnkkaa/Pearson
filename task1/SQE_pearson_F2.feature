Story-02
Feature: Send Email
	User should be able to send attachments
	User should be able to send empty messages
	Sent email should be available in Send category
Background:
	Given User MyNjuJob is successfully logged in
	And gmail homepage is displayed

Scenario1: New email is created
	Given 'Utwórz/Create' button is active
	When I click 'Utwórz/Create'
	Then 'new email' dialog should be poped-up

Scenario2: Attachment is attached
	Given 'new email' pop-up is displayed 
	And 'Email' field contains 'mynjujob@gmail.com'
	When I click 'add files/dodaj pliki' button
	And 'select file' dialog should be poped-up
	Then I select desired (xxx.txt) file from the directory
	And the file is successfully attached to the mail

Scenario3: Mail with attachment is sent
	Given desired file is attached to the email
	When I click 'send/wyślij' button
	Then the message with attachment is sent 
	And the message is delivered to the mailbox

Scenario4: Emtpy mail is sent
	Given 'Email' field contains 'mynjujob@gmail.com'
	And there is no content available
	And there is no title available
	When I click 'send/wyślij' button
	Then I confirmed sending of an empty email 
	And the empty message is sent 
	And the empty message is delivered to the mailbox

Scenario5: 'Send' category contains sent mails
	Given email with attachment had been sent
	And empty email had been sent
	When I select 'Send/Wysłane' folder
	Then sent email with attachment is available in the 'Send" category
	And empty email is available in the 'Send" category

