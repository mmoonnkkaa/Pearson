Story-01
Feature: Login to gmail.com 
	User has google account
	User is informed about wrong credentials
	User after successful login is redirected to gmail homepage
Background:
	Given New Job user is registered
	And valid email address is mynjujob@gmail 
	And the valid password is 'my@nju#job'
	
Scenario1: Welcome to gmail.com
	Given Login page is displayed
	And the 'Email' field contains 'mynjujob@gmail.com'
	When I press 'Continue/Dalej'
	Then I should be redirected to 'Welcome' page
	And I see given login name displayed

Scenario2: Succesfull Login to gmail.com
	Given Welcome page is displayed
	And the 'Password' field contains 'my@nju#job'
	When I press 'Continue/Dalej'
	Then I should be redirected to gmail homepage
	And I should see my emails

Scenario3: Failed Login to gmail - incorrect password
	Given Welcome page is displayed with correct email 'mynjujob@gmail.com'
	And the 'Password' field contains invalid 'my@nju#'
	When I press 'Login'
	Then I should be informed about wrong password
	
Scenario4: Failed Login to gmail.com - invalid email 
	Given Login page is displayed
	And the 'Email' field contains invalid 'mynju@gmail.com'
	When I press 'Continue/Dalej'
	Then I should be informed about wrong user name
